import React, { Component } from 'react'
// 插槽的作用
// 1.减少重复操作 （例如封装一个swiper组件-别人用的时候想要变成图文混排的）
// 2.一定程度上减少父子通信

// 插槽 在 子组件中 获取插槽数据 通过 {this.prpos.children} 
// 如果在插槽中写了多个div 可以通过{this.prpos.children[0]} 方式来获取第几个属性
export default class App extends Component {
    render() {
        return (
            <div>
                <Children>
                    <div>我是插槽数据</div>
                    <div>我是插槽</div>
                </Children>
            </div>
        )
    }
}

class Children extends Component {
    render() {
        return (
            <div>
                我是儿子组件
                <p>{this.props.children}</p>
                <hr></hr>
                <p>{this.props.children[0]}</p>
                <p>{this.props.children[1]}</p>
            </div>
        )
    }
}