import React, { Component } from 'react'

export default class FileDetail extends Component {
  render() {
    return (
      <div className='file_detail'>
        <h2>详情：</h2>
        <p>{this.props.detail}</p>
      </div>
    )
  }
}
