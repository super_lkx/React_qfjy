import React, { Component } from 'react'
import axios from 'axios';
import './index.css';

// context 解决兄弟组件通讯

// 先创建context对象
const GlobalContext = React.createContext();

// 注意点：
// 1.Provider表示的是供应商的意思 Consumer表示消费者
// 2.在使用 <GlobalContext.Consumer> 注意书写格式，需要写在render函数里面
// render(){
//     return (
//         <GlobalContext.Consumer>
//             { 
//                 (value)=>{
//                     return (
//                         <div>
//                             123455
//                             {value}
//                         </div>
//                     )
//                 }
//             }
//         </GlobalContext.Consumer>
//     )
// }



export default class App extends Component {
    constructor() {
        super()
        this.state = {
            filmList: [],
            detail: "",//内容
        }
        axios.get(`/test.json`).then((res) => {
            this.setState({
                filmList: res.data.data.films
            })
        })
    }
    render() {
        return (
            // Provider包裹全部兄弟2个组件- 使用关键字 value来创建一个对象 用于在其他Consumer中的回调函数中的参数
            <GlobalContext.Provider value={{
                content: this.state.detail,
                changeInfo: (value) => {
                    this.setState({
                        detail: value
                    })
                }
            }}>
                <div className='content_wrap'>
                    <div className='left_wrap'>
                        {
                            this.state.filmList.map(item => {
                                return <FilemItem {...item} key={item.filmId}></FilemItem>
                            })
                        }
                    </div>
                    <div className='right_wrap'>
                        <FileDetail></FileDetail>
                    </div>
                </div>
            </GlobalContext.Provider>
        )
    }
}

// 左侧组件
class FilemItem extends Component {
    handleItem = (e) => {
        // 调用 provider value 里面的方法 来改变数据-
        e.changeInfo(this.props.synopsis);
    }
    render() {
        return (
            <GlobalContext.Consumer>
                {
                    (value) => {
                        return (
                            <div className='movie_wrap' onClick={() => {
                                // 给事件传参的方法- 这个参数value 就是 获取provider 里面的value的全部对象；包括属性和方法
                                this.handleItem(value)
                            }}>
                                <h2>{this.props.name}</h2>
                                <img src={this.props.poster} alt={this.props.name} />
                            </div>
                        )
                    }
                }
            </GlobalContext.Consumer>
        )
    }
}
// 右侧内容组件
class FileDetail extends Component {   
    render() {
        return (
            <GlobalContext.Consumer>
                {
                    (value) => {
                        return (
                            <div className='file_detail'>
                                <h2>详情：</h2>
                                <p>{value.content}</p>
                            </div>
                        )
                    }
                }
            </GlobalContext.Consumer>
        )
    }
}

