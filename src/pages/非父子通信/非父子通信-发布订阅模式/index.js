import React, { Component } from 'react'
import axios from 'axios';
import './index.css';


// 最基础的订阅发布模式。
// 
var bus = {
    list:[],
    //定义一个： 订阅方法
    subscribe(callback){
        // console.log(callback);
        // 在上面生命一个list数组 用来存放 每次订阅来的回调函数，下面调用几次订阅我们就向数组中存放几次；
        this.list.push(callback);
    },
    // 定义一个： 发布方法
    // 我们的发布想要获取上面的 callback一共回调了几次 没法直接获取，所以需要一个公共值来代替；list
    publish(e){
        this.list.forEach(item=>{
            // 因为 list里面全是函数，那么需要让他自动执行；
            // 这里写的是 item 其实就是callback 必须存在 才自动去执行；
            item && item(e);
        })
    }
}

// 发布订阅模式 解决兄弟组件通讯
export default class App extends Component {
    constructor() {
        super()
        this.state = {
            filmList: [],
        }
        axios.get(`/test.json`).then((res) => {
            // console.log(res.data.data.films);
            this.setState({
                filmList: res.data.data.films
            })
        })
    }
    getInfo = (e) => {
        console.log(e);
        this.setState({
            detail: e
        })
    }
    render() {
        return (
            <div className='content_wrap'>
                <div className='left_wrap'>
                    {
                        this.state.filmList.map(item => {
                            return <FilemItem {...item} key={item.filmId}></FilemItem>
                        })
                    }
                </div>
                <div className='right_wrap'>
                    <FileDetail></FileDetail>
                </div>
            </div>
        )
    }
}

// 左侧组件
class FilemItem extends Component {
    handleItem = () => {
        bus.publish(this.props.synopsis);
    }
    render() {
        return (
            <div className='movie_wrap' onClick={this.handleItem}>
                <h2>{this.props.name}</h2>
                <img src={this.props.poster} alt="this.props.name" />
            </div>
        )
    }
}
// 右侧内容组件
class FileDetail extends Component {
    constructor(){
        super()
        this.state = {
            detail:""
        }
        bus.subscribe((info)=>{
            this.setState({
                detail:info
            })
        })
    }
    render() {
        return (
            <div className='file_detail'>
                <h2>详情：</h2>
                <p>{this.state.detail}</p>
            </div>
        )
    }
}

