import React, { Component } from 'react'

export default class App extends Component {
  render() {
    return (
      <div>index</div>
    )
  }
}

// 最基础的订阅发布模式。
// 
var bus = {
    list:[],
    //定义一个： 订阅方法
    subscribe(callback){
        // console.log(callback);
        // 在上面生命一个list数组 用来存放 每次订阅来的回调函数，下面调用几次订阅我们就向数组中存放几次；
        this.list.push(callback);
    },
    // 定义一个： 发布方法
    // 我们的发布想要获取上面的 callback一共回调了几次 没法直接获取，所以需要一个公共值来代替；list
    publish(e){
        this.list.forEach(item=>{
            // 因为 list里面全是函数，那么需要让他自动执行；
            // 这里写的是 item 其实就是callback 必须存在 才自动去执行；
            item && item(e);
        })
    }
}

// 先订阅 执行的时候 传递的参数为一个回调函数
bus.subscribe((value)=>{
    // console.log("执行第一次订阅");
    console.log(value)
});
bus.subscribe((value)=>{
    // console.log("执行第二次订阅")
    console.log(value)
});

// 执行 发布模式
// bus.publish();

// 使用异步操作
setTimeout(() => {
    bus.publish('男人看了沉默，女人看了流泪111');
}, 1000);
setTimeout(() => {
    bus.publish('男人看了沉默，女人看了流泪2222');
}, 2000);

setTimeout(() => {
    bus.publish('男人看了沉默，女人看了流泪3333');
}, 3000);