import React, { Component } from 'react'

export default class Filed extends Component {
    state = {
        values:'',
    }
    getValue = (e)=>{
        this.setState({
            values:e.target.value
        })
    }
    clearn =()=>{
        this.setState({
            values:''
        })
    }
    render() {
        return (
            <div>
                <span>{this.props.label}</span>
                <input type={this.props.type} onInput={this.getValue} value={this.state.values} />
            </div>
        )
    }
}
